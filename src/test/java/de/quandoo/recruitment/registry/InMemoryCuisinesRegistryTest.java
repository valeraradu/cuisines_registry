package de.quandoo.recruitment.registry;

import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.Customer;
import org.junit.Test;

public class InMemoryCuisinesRegistryTest {

    @Test
    public void testRegister() {

        InMemoryCuisinesRegistry cuisinesRegistry = new InMemoryCuisinesRegistry();

        cuisinesRegistry.register(new Customer("bob"), new Cuisine("german"));
        assert cuisinesRegistry.cuisineCustomers(new Cuisine("german")).size() == 1;
        assert cuisinesRegistry.customerCuisines(new Customer("bob")).size() == 1;
    }

    @Test
    public void testAddCuisineToExistingCustomer() {

        InMemoryCuisinesRegistry cuisinesRegistry = new InMemoryCuisinesRegistry();

        cuisinesRegistry.register(new Customer("bob"), new Cuisine("german"));
        cuisinesRegistry.register(new Customer("bob"), new Cuisine("french"));

        assert cuisinesRegistry.cuisineCustomers(new Cuisine("german")).size() == 1;
        assert cuisinesRegistry.customerCuisines(new Customer("bob")).size() == 2;
    }

    @Test
    public void testAddCustomerToExistingCuisine() {

        InMemoryCuisinesRegistry cuisinesRegistry = new InMemoryCuisinesRegistry();

        cuisinesRegistry.register(new Customer("bob"), new Cuisine("german"));
        cuisinesRegistry.register(new Customer("dorf"), new Cuisine("german"));

        assert cuisinesRegistry.cuisineCustomers(new Cuisine("german")).size() == 2;
        assert cuisinesRegistry.customerCuisines(new Customer("bob")).size() == 1;
    }

    @Test
    public void testAddExistingCustomerToExistingCuisine() {

        InMemoryCuisinesRegistry cuisinesRegistry = new InMemoryCuisinesRegistry();

        cuisinesRegistry.register(new Customer("bob"), new Cuisine("german"));
        cuisinesRegistry.register(new Customer("dorf"), new Cuisine("german"));

        cuisinesRegistry.register(new Customer("bob"), new Cuisine("french"));
        cuisinesRegistry.register(new Customer("dorf"), new Cuisine("french"));

        assert cuisinesRegistry.cuisineCustomers(new Cuisine("german")).size() == 2;
        assert cuisinesRegistry.cuisineCustomers(new Cuisine("french")).size() == 2;
        assert cuisinesRegistry.customerCuisines(new Customer("bob")).size() == 2;
        assert cuisinesRegistry.customerCuisines(new Customer("dorf")).size() == 2;

    }

    @Test
    public void testTopCuisines() {
        InMemoryCuisinesRegistry cuisinesRegistry = new InMemoryCuisinesRegistry();
        cuisinesRegistry.register(new Customer("bob"), new Cuisine("german"));
        cuisinesRegistry.register(new Customer("dorf"), new Cuisine("french"));
        cuisinesRegistry.register(new Customer("robert"), new Cuisine("french"));
        cuisinesRegistry.register(new Customer("c"), new Cuisine("french"));
        cuisinesRegistry.register(new Customer("martin"), new Cuisine("german"));

        cuisinesRegistry.register(new Customer("dorf"), new Cuisine("russian"));
        cuisinesRegistry.register(new Customer("dorf"), new Cuisine("asian"));
        cuisinesRegistry.register(new Customer("dorf"), new Cuisine("chinese"));
        cuisinesRegistry.register(new Customer("dorf"), new Cuisine("moldavian"));
        cuisinesRegistry.register(new Customer("dorf"), new Cuisine("poland"));
        cuisinesRegistry.register(new Customer("dorf"), new Cuisine("blabla"));

        assert cuisinesRegistry.topCuisines(2).contains(new Cuisine("german"));
        assert cuisinesRegistry.topCuisines(2).contains(new Cuisine("french"));
        assert cuisinesRegistry.topCuisines(2).size() == 2;

    }
}