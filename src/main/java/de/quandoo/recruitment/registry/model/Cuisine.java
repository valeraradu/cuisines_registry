package de.quandoo.recruitment.registry.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Cuisine {

    public List<Customer> getCustomers() {
        return customers;
    }

    public void setCustomers(List<Customer> customers) {
        this.customers = customers;
    }

    private List<Customer> customers = new ArrayList();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cuisine cuisine = (Cuisine) o;
        return Objects.equals(name, cuisine.name);
    }

    @Override
    public int hashCode() {

        return Objects.hash(name);
    }

    private final String name;

    public Cuisine(final String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

}
