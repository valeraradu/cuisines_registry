package de.quandoo.recruitment.registry;

import de.quandoo.recruitment.registry.api.CuisinesRegistry;
import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.Customer;

import java.util.*;

public class InMemoryCuisinesRegistry implements CuisinesRegistry {

    private List<Customer> customers = new ArrayList<>();
    private List<Cuisine> cuisines = new ArrayList<>();

    @Override
    public void register(final Customer userId, final Cuisine cuisine) {

        if(cuisines.indexOf(cuisine) == -1){
            cuisines.add(cuisine);
        }

        if(customers.indexOf(userId) == -1){
            customers.add(userId);
        }

        if(cuisines.get(cuisines.indexOf(cuisine)).getCustomers().indexOf(userId) == -1){
            cuisines.get(cuisines.indexOf(cuisine)).getCustomers().add(userId);
        }
        if (customers.get(customers.indexOf(userId)).getCuisines().indexOf(cuisines) == -1){
            customers.get(customers.indexOf(userId)).getCuisines().add(cuisine);
        }
    }

    @Override
    public List<Customer> cuisineCustomers(final Cuisine cuisine) {
        return cuisines.get(cuisines.indexOf(cuisine)).getCustomers();
    }

    @Override
    public List<Cuisine> customerCuisines(final Customer customer) {
        return customers.get(customers.indexOf(customer)).getCuisines();
    }

    @Override
    public List<Cuisine> topCuisines(final int n) {

         Collections.sort(cuisines,
                 Comparator.comparingInt(cuisine -> cuisine.getCustomers().size()));

         return cuisines.subList(cuisines.size()-n, cuisines.size());
    }
}
