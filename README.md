# Cuisines Registry


## Scaling in memory:


First of all please let me add a remark that adding a state to a module that is
supposed to be scalable(btw it is always a bad idea to add some kind of state to your application, not scalable, hard to update, hard to debug, bugs that are impossible to reproduce)
is an awfully wrong idea, probably you do not want to do this, please review your use case
that this design is supposed to solve and for future reference formulate your requirement as a user
story from perspective of user rather then technical specification,
but if all enumerated still does not convinced you my suggestion is to store data in an data structure
which supports hashing and in dependence of calculated hash dispatch request to an appropriate instance,
yet again implementing distributed in memory database is error prone and there are already good implementation of such kind of data storage,
 a better approach would be to abstract away source of data and this way system can use some kind of
 distributed cash like memcached or reddis and adding a fully persistent database if needed later would be almost trivial

## The story:

Cuisines Registry is an important part of Book-That-Table Inc. backend stack. It keeps in memory customer preferences for restaurant cuisines and is accessed by a bunch of components to register and retrieve data. 


The first iteration of this component was implemented by rather inexperienced developer and now may require some cleaning while new functionality is being added. But fortunately, according to his words: "Everything should work and please keep the test coverage as high as I did"


## Your tasks:
1. **[Important!]** Adhere to the boy scout rule. Leave your code better than you found it.
It is ok to change any code as long as the CuisinesRegistry interface remains unchanged.
2. Make is possible for customers to follow more than one cuisine (return multiple cuisines in de.quandoo.recruitment.registry.api.CuisinesRegistry#customerCuisines)
3. Implement de.quandoo.recruitment.registry.api.CuisinesRegistry#topCuisines - returning list of most popular (highest number of registered customers) ones
4. Create a short write up on how you would plan to scale this component to be able process in-memory billions of customers and millions of cuisines (Book-That-Table is already planning for galactic scale). (100 words max)

## Submitting your solution

+ Fork it to a private gitlab repository.
+ Put write up mentioned in point 4. into this file.
+ Send us a link to the repository, together with private ssh key that allows access (settings > repository > deploy keys).